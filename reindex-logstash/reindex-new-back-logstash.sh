#!/bin/bash
#  curl '10.3.41.123:9200/_cat/indices?v' | grep new_logstash- > list.txt
# thisscript.sh list.txt
# yellow open   logstash-2016.08.31       5   1      57164            0     20.3mb         20.3mb
 
TOTAL=`wc -l $1 | awk '{print $1}'`
COUNT=1

ESSERVER="10.3.41.123:9200"
SSDLOG="/root"

while [ $COUNT -le $TOTAL ]
do

LINES=`head -$COUNT $1 | tail -1`
LOGSTASHNAME=`echo $LINES | awk '{print $3}'`
OLDLOGSTASHNAME=`echo $LINES | awk '{print $3}' | cut -f2 -d _`

echo $LINES $LOGSTASHNAME $OLDLOGSTASHNAME
echo " "

### Start of logic

## Delete index dulu

curl -XDELETE 'http://'$ESSERVER'/'$OLDLOGSTASHNAME'/'

echo " "

###

curl -XPOST ''$ESSERVER'/_reindex' -d '{
 "source" : {
  "index" : "'$LOGSTASHNAME'"
 },
 "dest" : {
  "index" : "'$OLDLOGSTASHNAME'"
 }
}'

###

logger -p local7.info "Done push migration to server process $$ - Error code $? - $LINES"

####
# end of script.

  COUNT=$(( $COUNT + 1 ))


done
exit
